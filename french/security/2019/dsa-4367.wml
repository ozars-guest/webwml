#use wml::debian::translation-check translation="9b882a5d38bd6c3c5a8d79a39b0033bc189699c0" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Qualys Research Labs a découvert plusieurs vulnérabilités dans
systemd-journald. Deux défauts de corruption de mémoire, à l'aide
d'allocations contrôlées par l'attaquant en utilisant la fonction alloca
(<a href="https://security-tracker.debian.org/tracker/CVE-2018-16864">CVE-2018-16864</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2018-16865">CVE-2018-16865</a>)
et un défaut de lecture hors limites menant à une fuite d'informations
(<a href="https://security-tracker.debian.org/tracker/CVE-2018-16866">CVE-2018-16866</a>),
pourraient permettre à un attaquant de provoquer un déni de service ou
l'exécution de code arbitraire.</p>

<p>Vous trouverez plus de détails dans l'annonce de Qualys Security à
l'adresse : <a href="https://www.qualys.com/2019/01/09/system-down/system-down.txt">\
https://www.qualys.com/2019/01/09/system-down/system-down.txt</a>.</p>

<p>Pour la distribution stable (Stretch), ces problèmes ont été corrigés
dans la version 232-25+deb9u7.</p>

<p>Nous vous recommandons de mettre à jour vos paquets systemd.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de systemd, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/systemd">\
https://security-tracker.debian.org/tracker/systemd</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4367.data"
# $Id: $
