#use wml::debian::template title="Organizando um estande"
#use wml::debian::translation-check translation="2e3c0166e74b21c80857525f489ba5387969cc82" translation_maintainer="Paulo Henrique de Lima Santana (phls)"

<p> A tarefa mais importante para os membros do projeto representarem
o Projeto Debian em uma exibição é organizar um estande para o Debian. Isto
é bem fácil uma vez que é suficiente para um estande bastante simples
conseguir uma máquina rodando um sistema Debian GNU (Linux, Hurd ou *BSD)
e alguém para explicar como ele funciona e responder às dúvidas. É recomendado, 
porém, que pelo menos duas pessoas estejam no estande e que você use banners
e panfletos, além de mais algumas máquinas.

<p>Feiras e conferências geralmente oferecem um local onde
vários(as) desenvolvedores(as) podem se encontrar, mesmo que esteja localizado
longe da área principal da conferência ou da exposição.
Geralmente desenvolvedores(as) estrangeiros(as) são convidados(as) para dar uma
palestra, então até essas pessoas podem ser encontradas por lá.
Além disso, é sempre divertido ir a um bar com outros(as) desenvolvedores(as)
e apenas conversar sobre como melhorar o cache de inode do Linux ou sobre a
resolução de dependências do dpkg.

<p>Em muitos eventos, as pessoas criam páginas web adicionais e listas
de discussão para ajudá-las a planejar e organizar a presença do Projeto Debian. 
Por favor confira os links para &ldquo;coordenação&rdquo; em nossas páginas para
os próximos eventos e os eventos passados, para ter uma ideia sobre isso.
Por favor, leia também nossa <a href="checklist">checklist</a>.

<h3><a name="ml">Listas de Discussão</a></h3>

<p>O Projeto Debian oferece listas de discussão para coordenar
a participação do Debian em vários eventos:

<ul>
  <li> <a href="https://alioth-lists.debian.net/cgi-bin/mailman/listinfo/debian-br-eventos">debian-br-eventos</a>,
       dedicada a eventos no Brazil.
  <li> <a href="https://lists.debian.org/debian-dug-in/">debian-dug-in</a>,
       dedicada a eventos na Índia.
  <li> <a href="https://lists.debian.org/debian-events-eu/">debian-events-eu</a>,
       dedicada a eventos na Europa e europeus.
  <li> <a href="https://lists.debian.org/debian-events-ha/">debian-events-ha</a>,
       dedicada a eventos na América Hispânica.
  <li> <a href="https://lists.debian.org/debian-events-na/">debian-events-na</a>,
       dedicada a eventos na América do Norte e eventos neste continente.
  <li> <a href="https://lists.debian.org/debian-events-nl/">debian-events-nl</a>,
       dedicada a eventos nos Países Baixos.
</ul>

<p>Para se inscrever para qualquer uma destas listas, visite nossa <a
href="$(HOME)/MailingLists/subscribe">página de inscrições</a>.


